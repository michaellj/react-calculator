// libraries
import React from 'react';
import ReactDOM from 'react-dom';

// style
import './index.css';

// components
import App from './components/App/App';

// render to client's screen
ReactDOM.render(
	<React.StrictMode>
		<App />
	</React.StrictMode>,
	document.querySelector('#root')
);

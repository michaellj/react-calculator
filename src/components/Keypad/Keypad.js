// libraries
import React from 'react';
import PropTypes from 'prop-types';

// import components
import Key from '../Key/Key';

// styles
import './Keypad.css';

// initialize props
const _defaultProps = {
	operators: [],
	numbers: [],
};

const _propTypes = {
	operators: PropTypes.array.isRequired,
	numbers: PropTypes.array.isRequired,
	callOperator: PropTypes.func.isRequired,
	setOperator: PropTypes.func.isRequired,
	updateDisplay: PropTypes.func.isRequired,
};

// create component
const Keypad = ({
	operators,
	numbers,
	callOperator,
	setOperator,
	updateDisplay,
}) => {
	const renderNumberKeys = () =>
		numbers.map((number, i) => (
			<Key
				key={number}
				keyAction={updateDisplay}
				keyType='number-key'
				keyValue={number}
			/>
		));

	const renderOperatorKeys = () =>
		operators.map((operator, i) => (
			<Key
				key={operator}
				keyAction={setOperator}
				keyType='operator-key'
				keyValue={operator}
			/>
		));

	return (
		<div className='keypad-container'>
			<div className='numbers-container'>{renderNumberKeys()}</div>
			<div className='operators-container'>{renderOperatorKeys()}</div>
			<div className='submit-container'>
				<Key keyAction={callOperator} keyType='submit-key' keyValue='=' />
			</div>
		</div>
	);
};

Keypad.defaultProps = _defaultProps;
Keypad.propTypes = _propTypes;

// make component available
export default Keypad;

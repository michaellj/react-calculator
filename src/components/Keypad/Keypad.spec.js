// libraries
import React from 'react';
import { shallow, mount } from 'enzyme';

// import components
import Keypad from './Keypad';
import Key from '../Key/Key';

// suite tests
describe('Keypad', () => {
	let wrapper;

	beforeEach(() => {
		wrapper = shallow(
			<Keypad
				operators={[]}
				numbers={[]}
				callOperator={jest.fn()}
				setOperator={jest.fn()}
				updateDisplay={jest.fn()}
			/>
		);
	});

	it('should render correctly', () => expect(wrapper).toMatchSnapshot());

	it('should have a class name of keypad-container', () => {
		expect(wrapper.hasClass('keypad-container')).toEqual(true);
	});

	it('should render 4 div', () => {
		expect(wrapper.find('div')).toHaveLength(4);
	});

	it('should render an instance of the Key component for each index of numbers, operators and the submit key', () => {
		const numbers = ['0', '1'];
		const operators = ['+', '-'];
		const submit = 1;
		const keyTotal = numbers.length + operators.length + submit;
		wrapper.setProps({ numbers, operators });

		expect(wrapper.find('Key').length).toEqual(keyTotal);
	});
});

describe('mounted Keypad', () => {
	let wrapper;

	beforeEach(() => {
		wrapper = mount(
			<Keypad
				callOperator={jest.fn()}
				numbers={[]}
				operators={[]}
				setOperator={jest.fn()}
				updateDisplay={jest.fn()}
			/>
		);
	});

	it('renders the values of numbers', () => {
		wrapper.setProps({ numbers: ['0', '1', '2'] });
		expect(wrapper.find('.numbers-container').text()).toEqual('012');
	});

	it('renders the values of operators', () => {
		wrapper.setProps({ operators: ['+', '-', '*', '/'] });
		expect(wrapper.find('.operators-container').text()).toEqual('+-*/');
	});
});

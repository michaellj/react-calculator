// libraries
import React from 'react';
import PropTypes from 'prop-types';

// styles
import './Display.css';

// props configuration
const _propTypes = {
	displayValue: PropTypes.string.isRequired,
};

const _defaultProps = {
	displayValue: '0',
};

// create component
const Display = ({ displayValue }) => {
	return (
		<div className='display-container'>
			<p className='display-value'>{displayValue}</p>
		</div>
	);
};

Display.defaultProps = _defaultProps;

Display.propTypes = _propTypes;

// make component available
export default Display;

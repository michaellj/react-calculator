// libraries
import React from 'react';
import { shallow } from 'enzyme';

// import components
import Display from './Display';

// suite tests
describe('Display', () => {
	let wrapper;

	beforeEach(() => {
		wrapper = shallow(<Display displayValue={''} />);
	});

	it('should render correctly', () => {
		expect(wrapper).toMatchSnapshot();
	});

	it('should render a div', () => {
		expect(wrapper.find('div').length).toEqual(1);
	});

	it('should have a class display-container', () => {
		expect(wrapper.hasClass('display-container')).toEqual(true);
	});

	it('should contain a paragraph with the class display-value', () => {
		const paragraph = wrapper.find('p');
		expect(paragraph).toHaveLength(1);
		expect(paragraph.hasClass('display-value')).toEqual(true);
	});

	it('should render the value of the displayValue props ', () => {
		const testValue = 'test';
		wrapper.setProps({ displayValue: testValue });
		expect(wrapper.text()).toEqual(testValue);
	});
});

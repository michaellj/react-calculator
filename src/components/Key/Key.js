// libraries
import React from 'react';
import PropTypes from 'prop-types';

// style
import './Key.css';

// initialize props
const _defaultProps = {
	keyType: '',
	keyValue: '',
};
const _propTypes = {
	keyAction: PropTypes.func.isRequired,
	keyType: PropTypes.string.isRequired,
	keyValue: PropTypes.string.isRequired,
};

// create component
const Key = ({ keyAction, keyType, keyValue }) => {
	return (
		<div
			className={`key-container ${keyType}`}
			onClick={() => keyAction(keyValue)}>
			<p className='key-value'>{keyValue}</p>
		</div>
	);
};

Key.defaultProps = _defaultProps;
Key.propTypes = _propTypes;

// make component available
export default Key;

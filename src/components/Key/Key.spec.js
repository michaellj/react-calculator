// libraries
import React from 'react';
import { shallow } from 'enzyme';

// components
import Key from './Key';
import Calculator from '../Calculator/Calculator';

// suite test
describe('Key', () => {
	let wrapper;

	beforeEach(() => {
		wrapper = shallow(<Key keyAction={jest.fn()} keyType={''} keyValue={''} />);
	});

	it('should render correctly', () => {
		expect(wrapper).toMatchSnapshot();
	});

	it('should render a div', () => {
		expect(wrapper.find('div').length).toEqual(1);
	});

	it('should render a <p></p>', () => {
		expect(wrapper.find('p').length).toEqual(1);
	});

	it('should have a css class key-container', () => {
		expect(wrapper.hasClass('key-container')).toEqual(true);
	});

	it('should have a div with a css class key-value', () => {
		expect(wrapper.find('.key-value').length).toEqual(1);
	});

	it('should render the value of keyValue', () => {
		wrapper.setProps({ keyValue: 'test' });
		expect(wrapper.text()).toEqual('test');
	});
});

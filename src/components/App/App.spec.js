// libraries
import React from 'react';
import { shallow } from 'enzyme';

// components
import App from './App';
import Calculator from '../Calculator/Calculator';

// suite test
describe('App', () => {
	let wrapper;

	beforeEach(() => (wrapper = shallow(<App />)));

	it('should render correctly', () => {
		expect(wrapper).toMatchSnapshot();
	});

	it('should render a div', () => {
		expect(wrapper.find('div').length).toEqual(1);
	});

	it('', () => {
		expect(wrapper.containsMatchingElement(<Calculator />)).toEqual(true);
	});
});

// library
import React from 'react';

// import components
import Calculator from '../Calculator/Calculator';

// styles
import './App.css';

// create components
const App = () => (
	<div className='app-container'>
		<Calculator />
	</div>
);

// make component available
export default App;
